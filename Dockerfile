FROM registry.gitlab.com/hiproz/centos-systemd-docker

LABEL vendor="itranscloud.com"

COPY entrypoint.sh /
COPY bt_install.sh /
COPY need_update /

# prepare for software
RUN yum -y update && yum install -y yes wget procps curl iproute2 build-essential \
    && yum clean all && rm -rf /tmp/* /var/cache/yum/* \
    && chmod +x /entrypoint.sh 

WORKDIR /www

VOLUME [ "/www" ]

EXPOSE 8888 888 80 443 21 22 3306

ENTRYPOINT ["/entrypoint.sh"]
