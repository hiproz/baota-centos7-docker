# baota-centos7-docker
a baota docker based centos7,use the latest version of [www.bt.cn](bt.cn).

## start docker
```
docker run -tid --name baota --restart always -v /your-local-dir:/www -p your-local-port:8888 registry.gitlab.com/hiproz/baota-centos7-docker
```

## uniqueness
- The biggest feature of this version is that all app paths are mapped to local paths through delayed installation. When docker is deleted, all app data include mysql database data will not be lost.
- support reinstall the baota but not needed reinstall the docker.

## usage
### installation log
tail -f run.log

note：it will be a long time that install the baota after the booting of docker, please check the run.log for confirming the finish of installation.

### web root
./webroot

### application path
./server
all application are int this path, such as mysql, php, nginx etc.

### data path
./server/data

### update baota
if you want reinstall the baota out of the baota docker:
- update the bt_install.sh, edit the content as needed 
- `echo '1' > need_update`

# default login path and password
vi ./default.txt
the port in the default.txt is the docker inner port, so you should use your mapped port of the server.
