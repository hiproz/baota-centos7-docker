#!/bin/bash
if [ ! -f "/www/bt_install.sh" ]; then
  cp /bt_install.sh /www
  chmod +x /www/bt_install.sh
fi

if [ ! -f "/www/need_update" ]; then
  cp /need_update /www
fi

flag=$(cat /www/need_update)
if [ $flag == '1' ];then
    echo update the baota >> run.log
    /www/bt_install.sh >> run.log
    bt default >> /www/default.txt

    echo '0' > /www/need_update
fi

date >> /www/run.log
bt restart >> /www/run.log
tail -f /dev/null
